﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntlrCSharp.Models
{
    public class Basket
    {
        public Basket(List<Product> products)
        {
            Products = products;
        }

        public List<Product> Products { get; }
        public decimal Discount => Products.Sum(x => x.Discount);
        public decimal OriginalPrice => Products.Sum(x => x.OriginalPrice);
        public decimal TotalPrice => Products.Sum(x => x.TotalPrice);
    }
}
