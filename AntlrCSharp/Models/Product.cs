﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntlrCSharp.Models
{
    public class Product
    {

        public Product(string name, decimal originalPrice)
        {
            Name = name;
            OriginalPrice = originalPrice;
        }

        public string Name { get; }
        public decimal OriginalPrice { get; }
        public decimal Discount { get; set; }
        public decimal TotalPrice => OriginalPrice - Discount;
        public void ApplyDiscount(int percentage)
        {
            if (percentage > 100 || percentage < 0)
                throw new ArgumentOutOfRangeException(nameof(percentage));

            Discount = OriginalPrice * percentage / 100;
        }
    }
}
