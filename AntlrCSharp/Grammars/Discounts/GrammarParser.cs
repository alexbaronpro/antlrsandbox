﻿using Antlr4.Runtime;
using AntlrCSharp.Grammars.Discounts.Expressions;
using AntlrCSharp.Grammars.Discounts.Visitors;

namespace AntlrCSharp.Grammars.Discounts
{
    public class GrammarParser
    {
        private readonly RulesVisitor _rulesVisitor = new RulesVisitor();
        public RuleExpression Parse(string discount)
        {
            var charStream = new AntlrInputStream(discount);
            var lexer = new DiscountsLexer(charStream);
            var tokenStream = new CommonTokenStream(lexer);
            var parser = new DiscountsParser(tokenStream);
            var tree = parser.rules();
            var rules = tree.Accept(_rulesVisitor);

            return rules;
        }
    }
}
