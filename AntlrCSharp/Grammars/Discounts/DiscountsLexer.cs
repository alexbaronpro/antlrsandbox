//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     ANTLR Version: 4.13.1
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generated from c:/Users/ylatif/source/repos/antlrsandbox/AntlrCSharp/Grammars/Discounts/DiscountsLexer.g4 by ANTLR 4.13.1

// Unreachable code detected
#pragma warning disable 0162
// The variable '...' is assigned but its value is never used
#pragma warning disable 0219
// Missing XML comment for publicly visible type or member '...'
#pragma warning disable 1591
// Ambiguous reference in cref attribute
#pragma warning disable 419

using System;
using System.IO;
using System.Text;
using Antlr4.Runtime;
using Antlr4.Runtime.Atn;
using Antlr4.Runtime.Misc;
using DFA = Antlr4.Runtime.Dfa.DFA;

[System.CodeDom.Compiler.GeneratedCode("ANTLR", "4.13.1")]
[System.CLSCompliant(false)]
public partial class DiscountsLexer : Lexer {
	protected static DFA[] decisionToDFA;
	protected static PredictionContextCache sharedContextCache = new PredictionContextCache();
	public const int
		COMMA=1, AND=2, OR=3, FOR=4, BASKET=5, WITH=6, ALL=7, APPLY=8, DISCOUNT=9, 
		TO=10, MORE_Q=11, PERCENT=12, INT=13, ID=14, SPACE=15;
	public static string[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static string[] modeNames = {
		"DEFAULT_MODE"
	};

	public static readonly string[] ruleNames = {
		"COMMA", "AND", "OR", "FOR", "BASKET", "WITH", "ALL", "APPLY", "DISCOUNT", 
		"TO", "MORE_Q", "PERCENT", "INT", "ID", "SPACE", "A", "B", "C", "D", "E", 
		"F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", 
		"T", "U", "V", "W", "X", "Y", "Z"
	};


	public DiscountsLexer(ICharStream input)
	: this(input, Console.Out, Console.Error) { }

	public DiscountsLexer(ICharStream input, TextWriter output, TextWriter errorOutput)
	: base(input, output, errorOutput)
	{
		Interpreter = new LexerATNSimulator(this, _ATN, decisionToDFA, sharedContextCache);
	}

	private static readonly string[] _LiteralNames = {
		null, "','", null, null, null, null, null, null, null, null, null, null, 
		"'%'"
	};
	private static readonly string[] _SymbolicNames = {
		null, "COMMA", "AND", "OR", "FOR", "BASKET", "WITH", "ALL", "APPLY", "DISCOUNT", 
		"TO", "MORE_Q", "PERCENT", "INT", "ID", "SPACE"
	};
	public static readonly IVocabulary DefaultVocabulary = new Vocabulary(_LiteralNames, _SymbolicNames);

	[NotNull]
	public override IVocabulary Vocabulary
	{
		get
		{
			return DefaultVocabulary;
		}
	}

	public override string GrammarFileName { get { return "DiscountsLexer.g4"; } }

	public override string[] RuleNames { get { return ruleNames; } }

	public override string[] ChannelNames { get { return channelNames; } }

	public override string[] ModeNames { get { return modeNames; } }

	public override int[] SerializedAtn { get { return _serializedATN; } }

	static DiscountsLexer() {
		decisionToDFA = new DFA[_ATN.NumberOfDecisions];
		for (int i = 0; i < _ATN.NumberOfDecisions; i++) {
			decisionToDFA[i] = new DFA(_ATN.GetDecisionState(i), i);
		}
	}
	private static int[] _serializedATN = {
		4,0,15,208,6,-1,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
		6,2,7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,2,12,7,12,2,13,7,13,2,14,
		7,14,2,15,7,15,2,16,7,16,2,17,7,17,2,18,7,18,2,19,7,19,2,20,7,20,2,21,
		7,21,2,22,7,22,2,23,7,23,2,24,7,24,2,25,7,25,2,26,7,26,2,27,7,27,2,28,
		7,28,2,29,7,29,2,30,7,30,2,31,7,31,2,32,7,32,2,33,7,33,2,34,7,34,2,35,
		7,35,2,36,7,36,2,37,7,37,2,38,7,38,2,39,7,39,2,40,7,40,1,0,1,0,1,1,1,1,
		1,1,1,1,1,2,1,2,1,2,1,3,1,3,1,3,1,3,1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,5,1,
		5,1,5,1,5,1,5,1,6,1,6,1,6,1,6,1,7,1,7,1,7,1,7,1,7,1,7,1,8,1,8,1,8,1,8,
		1,8,1,8,1,8,1,8,1,8,1,9,1,9,1,9,1,10,1,10,1,10,1,10,1,10,1,11,1,11,1,12,
		4,12,139,8,12,11,12,12,12,140,1,13,1,13,5,13,145,8,13,10,13,12,13,148,
		9,13,1,14,4,14,151,8,14,11,14,12,14,152,1,14,1,14,1,15,1,15,1,16,1,16,
		1,17,1,17,1,18,1,18,1,19,1,19,1,20,1,20,1,21,1,21,1,22,1,22,1,23,1,23,
		1,24,1,24,1,25,1,25,1,26,1,26,1,27,1,27,1,28,1,28,1,29,1,29,1,30,1,30,
		1,31,1,31,1,32,1,32,1,33,1,33,1,34,1,34,1,35,1,35,1,36,1,36,1,37,1,37,
		1,38,1,38,1,39,1,39,1,40,1,40,0,0,41,1,1,3,2,5,3,7,4,9,5,11,6,13,7,15,
		8,17,9,19,10,21,11,23,12,25,13,27,14,29,15,31,0,33,0,35,0,37,0,39,0,41,
		0,43,0,45,0,47,0,49,0,51,0,53,0,55,0,57,0,59,0,61,0,63,0,65,0,67,0,69,
		0,71,0,73,0,75,0,77,0,79,0,81,0,1,0,30,1,0,48,57,4,0,35,35,65,90,95,95,
		97,122,6,0,35,35,46,46,48,57,65,90,95,95,97,122,3,0,9,10,12,13,32,32,2,
		0,65,65,97,97,2,0,66,66,98,98,2,0,67,67,99,99,2,0,68,68,100,100,2,0,69,
		69,101,101,2,0,70,70,102,102,2,0,71,71,103,103,2,0,72,72,104,104,2,0,73,
		73,105,105,2,0,74,74,106,106,2,0,75,75,107,107,2,0,76,76,108,108,2,0,77,
		77,109,109,2,0,78,78,110,110,2,0,79,79,111,111,2,0,80,80,112,112,2,0,81,
		81,113,113,2,0,82,82,114,114,2,0,83,83,115,115,2,0,84,84,116,116,2,0,85,
		85,117,117,2,0,86,86,118,118,2,0,87,87,119,119,2,0,88,88,120,120,2,0,89,
		89,121,121,2,0,90,90,122,122,184,0,1,1,0,0,0,0,3,1,0,0,0,0,5,1,0,0,0,0,
		7,1,0,0,0,0,9,1,0,0,0,0,11,1,0,0,0,0,13,1,0,0,0,0,15,1,0,0,0,0,17,1,0,
		0,0,0,19,1,0,0,0,0,21,1,0,0,0,0,23,1,0,0,0,0,25,1,0,0,0,0,27,1,0,0,0,0,
		29,1,0,0,0,1,83,1,0,0,0,3,85,1,0,0,0,5,89,1,0,0,0,7,92,1,0,0,0,9,96,1,
		0,0,0,11,103,1,0,0,0,13,108,1,0,0,0,15,112,1,0,0,0,17,118,1,0,0,0,19,127,
		1,0,0,0,21,130,1,0,0,0,23,135,1,0,0,0,25,138,1,0,0,0,27,142,1,0,0,0,29,
		150,1,0,0,0,31,156,1,0,0,0,33,158,1,0,0,0,35,160,1,0,0,0,37,162,1,0,0,
		0,39,164,1,0,0,0,41,166,1,0,0,0,43,168,1,0,0,0,45,170,1,0,0,0,47,172,1,
		0,0,0,49,174,1,0,0,0,51,176,1,0,0,0,53,178,1,0,0,0,55,180,1,0,0,0,57,182,
		1,0,0,0,59,184,1,0,0,0,61,186,1,0,0,0,63,188,1,0,0,0,65,190,1,0,0,0,67,
		192,1,0,0,0,69,194,1,0,0,0,71,196,1,0,0,0,73,198,1,0,0,0,75,200,1,0,0,
		0,77,202,1,0,0,0,79,204,1,0,0,0,81,206,1,0,0,0,83,84,5,44,0,0,84,2,1,0,
		0,0,85,86,3,31,15,0,86,87,3,57,28,0,87,88,3,37,18,0,88,4,1,0,0,0,89,90,
		3,59,29,0,90,91,3,65,32,0,91,6,1,0,0,0,92,93,3,41,20,0,93,94,3,59,29,0,
		94,95,3,65,32,0,95,8,1,0,0,0,96,97,3,33,16,0,97,98,3,31,15,0,98,99,3,67,
		33,0,99,100,3,51,25,0,100,101,3,39,19,0,101,102,3,69,34,0,102,10,1,0,0,
		0,103,104,3,75,37,0,104,105,3,47,23,0,105,106,3,69,34,0,106,107,3,45,22,
		0,107,12,1,0,0,0,108,109,3,31,15,0,109,110,3,53,26,0,110,111,3,53,26,0,
		111,14,1,0,0,0,112,113,3,31,15,0,113,114,3,61,30,0,114,115,3,61,30,0,115,
		116,3,53,26,0,116,117,3,79,39,0,117,16,1,0,0,0,118,119,3,37,18,0,119,120,
		3,47,23,0,120,121,3,67,33,0,121,122,3,35,17,0,122,123,3,59,29,0,123,124,
		3,71,35,0,124,125,3,57,28,0,125,126,3,69,34,0,126,18,1,0,0,0,127,128,3,
		69,34,0,128,129,3,59,29,0,129,20,1,0,0,0,130,131,3,55,27,0,131,132,3,59,
		29,0,132,133,3,65,32,0,133,134,3,39,19,0,134,22,1,0,0,0,135,136,5,37,0,
		0,136,24,1,0,0,0,137,139,7,0,0,0,138,137,1,0,0,0,139,140,1,0,0,0,140,138,
		1,0,0,0,140,141,1,0,0,0,141,26,1,0,0,0,142,146,7,1,0,0,143,145,7,2,0,0,
		144,143,1,0,0,0,145,148,1,0,0,0,146,144,1,0,0,0,146,147,1,0,0,0,147,28,
		1,0,0,0,148,146,1,0,0,0,149,151,7,3,0,0,150,149,1,0,0,0,151,152,1,0,0,
		0,152,150,1,0,0,0,152,153,1,0,0,0,153,154,1,0,0,0,154,155,6,14,0,0,155,
		30,1,0,0,0,156,157,7,4,0,0,157,32,1,0,0,0,158,159,7,5,0,0,159,34,1,0,0,
		0,160,161,7,6,0,0,161,36,1,0,0,0,162,163,7,7,0,0,163,38,1,0,0,0,164,165,
		7,8,0,0,165,40,1,0,0,0,166,167,7,9,0,0,167,42,1,0,0,0,168,169,7,10,0,0,
		169,44,1,0,0,0,170,171,7,11,0,0,171,46,1,0,0,0,172,173,7,12,0,0,173,48,
		1,0,0,0,174,175,7,13,0,0,175,50,1,0,0,0,176,177,7,14,0,0,177,52,1,0,0,
		0,178,179,7,15,0,0,179,54,1,0,0,0,180,181,7,16,0,0,181,56,1,0,0,0,182,
		183,7,17,0,0,183,58,1,0,0,0,184,185,7,18,0,0,185,60,1,0,0,0,186,187,7,
		19,0,0,187,62,1,0,0,0,188,189,7,20,0,0,189,64,1,0,0,0,190,191,7,21,0,0,
		191,66,1,0,0,0,192,193,7,22,0,0,193,68,1,0,0,0,194,195,7,23,0,0,195,70,
		1,0,0,0,196,197,7,24,0,0,197,72,1,0,0,0,198,199,7,25,0,0,199,74,1,0,0,
		0,200,201,7,26,0,0,201,76,1,0,0,0,202,203,7,27,0,0,203,78,1,0,0,0,204,
		205,7,28,0,0,205,80,1,0,0,0,206,207,7,29,0,0,207,82,1,0,0,0,4,0,140,146,
		152,1,0,1,0
	};

	public static readonly ATN _ATN =
		new ATNDeserializer().Deserialize(_serializedATN);


}
