﻿using AntlrCSharp.Grammars.Discounts.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntlrCSharp.Grammars.Discounts.Visitors
{
    public class ProductsVisitor : DiscountsParserBaseVisitor<ProductsExpression>
    {
        public override ProductsExpression VisitProducts(DiscountsParser.ProductsContext context)
        {
            var products = context.product()
                .Select(x => x.GetText())
                .ToList();

            return new ProductsExpression(products);
        }
    }
}
