﻿using AntlrCSharp.Grammars.Discounts.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntlrCSharp.Grammars.Discounts.Visitors
{
    public class ApplyVisitor : DiscountsParserBaseVisitor<ApplyExpression>
    {
        private readonly ProductsVisitor _productsVisitor = new ProductsVisitor();
        public override ApplyExpression VisitApply(DiscountsParser.ApplyContext context)
        {
            var percentage = int.Parse(context.INT().GetText());
            if (context.products() == null)
            {
                return new ApplyExpression(percentage);
            }
            return new ApplyExpression(percentage, context.products().Accept(_productsVisitor));
        }
    }
}
