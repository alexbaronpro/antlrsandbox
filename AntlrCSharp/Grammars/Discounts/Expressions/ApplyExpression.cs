﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntlrCSharp.Grammars.Discounts.Expressions
{
    public class ApplyExpression
    {
        public ApplyExpression(int percentage)
        {
            Percentage = percentage;
        }

        public ApplyExpression(int percentage, ProductsExpression productsExpression)
        {
            Percentage = percentage;
            ProductsExpression = productsExpression;
        }

        public int Percentage { get; }

        public ProductsExpression ProductsExpression { get; }
    }
}
