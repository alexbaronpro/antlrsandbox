grammar CIM;

expr: comparisonExpr;

comparisonExpr
    : arithmeticExpr '==' arithmeticExpr
    ;

arithmeticExpr
    : arithmeticExpr ('+' | '-') term
    | term
    ;

term
    : term ('*' | '/') factor
    | factor
    ;

factor
    : NUMBER
    | IDENTIFIER
    | IDENTIFIER '.' IDENTIFIER '.' IDENTIFIER '.' IDENTIFIER
    | '(' arithmeticExpr ')'
    ;

NUMBER: [0-9]+;
IDENTIFIER: [a-zA-Z_][a-zA-Z0-9_]*;
WS: [ \t\r\n]+ -> skip;