﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntlrCSharp.Grammars.CIM
{
    public class CIMEvaluator : CIMBaseVisitor<int>
    {
        private DateTime start;
        private DateTime end;
        private string type;

        public CIMEvaluator(DateTime start, DateTime end)
        {
            this.start = start;
            this.end = end;
        }

        public override int VisitComparisonExpr(CIMParser.ComparisonExprContext context)
        {
            int left = Visit(context.arithmeticExpr(0));
            int right = Visit(context.arithmeticExpr(1));
            return left == right ? 1 : 0;
        }

        public override int VisitArithmeticExpr(CIMParser.ArithmeticExprContext context)
        {
            if (context.ChildCount == 1)
            {
                return Visit(context.term());
            }
            else
            {
                int left = Visit(context.arithmeticExpr());
                int right = Visit(context.term());
                if (context.GetChild(1).GetText() == "+")
                    return left + right;
                else
                    return left - right;
            }
        }

        public override int VisitTerm(CIMParser.TermContext context)
        {
            if (context.ChildCount == 1)
            {
                return Visit(context.factor());
            }
            else
            {
                int left = Visit(context.term());
                int right = Visit(context.factor());
                if (context.GetChild(1).GetText() == "*")
                    return left * right;
                else
                    return left / right;
            }
        }

        public override int VisitFactor(CIMParser.FactorContext context)
        {
            if (context.NUMBER() != null)
            {
                return int.Parse(context.NUMBER().GetText());
            }
            else if (context.IDENTIFIER().Length == 4)
            {
                // Access the values from your data model
                string obj = context.IDENTIFIER(0).GetText();
                string property1 = context.IDENTIFIER(1).GetText();
                string property2 = context.IDENTIFIER(2).GetText();
                string property3 = context.IDENTIFIER(3).GetText();

                if (obj == "period" && property1 == "timeInterval")
                {
                    if (property2 == "end" && property3 == "Day")
                    {
                        return end.Day;
                    }
                    else if (property2 == "start" && property3 == "Day")
                    {
                        return start.Day;
                    }
                }

                throw new Exception("Unknown identifier");
            }
            else
            {
                return Visit(context.arithmeticExpr());
            }
        }
    }
}
