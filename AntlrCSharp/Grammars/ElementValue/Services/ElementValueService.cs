﻿using AntlrCSharp.Grammars.ElementValue.Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntlrCSharp.Grammars.ElementValue.Services
{
    public class ElementValueService
    {
        private readonly ElementHasValueParser _elementHasValueParser = new();
        private readonly ElementEqualsValuesParser _elementEqualsValueParser = new();

        public bool ExecuteHasValue(string field, string value, string expression)
        {
            var expr = _elementHasValueParser.Parse(expression);

            return expr.Element.Equals(field) && expr.Value.Equals(value);
        }

        public bool ExecuteEqualsValues(string value, string expression) 
        {
            var expr = _elementEqualsValueParser.Parse(expression);

            return expr.Equals ? 
                expr.ValuesExpression.Values.Contains(value) :
                !expr.ValuesExpression.Values.Contains(value);
        }
    }
}
