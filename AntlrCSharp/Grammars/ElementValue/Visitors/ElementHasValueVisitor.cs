﻿using Antlr4.Runtime.Misc;
using AntlrCSharp.Grammars.ElementValue.Expressions;

namespace AntlrCSharp.Grammars.ElementValue.Visitors
{
    public class ElementHasValueVisitor : ElementValueParserBaseVisitor<ElementHasValueExpression>
    {
        public override ElementHasValueExpression VisitElementHasValueExpr([NotNull] ElementValueParser.ElementHasValueExprContext context)
        {
            var parsedField = context.field().GetText();
            var parsedValue = context.value().GetText().Trim('"');

            return new ElementHasValueExpression(parsedField, parsedValue);
        }
    }
}
