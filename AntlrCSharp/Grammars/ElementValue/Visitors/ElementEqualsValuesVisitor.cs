﻿using Antlr4.Runtime.Misc;
using AntlrCSharp.Grammars.ElementValue.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntlrCSharp.Grammars.ElementValue.Visitors
{
    public class ElementEqualsValuesVisitor : ElementValueParserBaseVisitor<ElementEqualsValuesExpression>
    {
        private readonly ValuesVisitor _valuesVisitor = new ValuesVisitor();

        public override ElementEqualsValuesExpression VisitElementEqualsValuesExpr([NotNull] ElementValueParser.ElementEqualsValuesExprContext context)
        {
            var field = context.field().GetText();
            var equals = context.EQUALS() != null;
            return new ElementEqualsValuesExpression(equals, field, context.values().Accept(_valuesVisitor));
        }
    }
}
