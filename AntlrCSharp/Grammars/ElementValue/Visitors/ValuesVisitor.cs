﻿using Antlr4.Runtime.Misc;
using AntlrCSharp.Grammars.ElementValue.Expressions;

namespace AntlrCSharp.Grammars.ElementValue.Visitors
{
    public class ValuesVisitor : ElementValueParserBaseVisitor<ValuesExpression>
    {
        public override ValuesExpression VisitValues([NotNull] ElementValueParser.ValuesContext context)
        {
            var values = context.value()
                .Select(x => x.GetText().Trim('"'))
                .ToList();

            return new ValuesExpression(values);
        }
    }
}
