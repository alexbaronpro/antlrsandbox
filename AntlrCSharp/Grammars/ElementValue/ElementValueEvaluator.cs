﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntlrCSharp.Grammars.ElementValue
{
    public class ElementValueEvaluator : ElementValueParserBaseVisitor<bool>
    {
        private string _field;
        private string _value;

        public ElementValueEvaluator(string field, string value)
        {
            _field = field;
            _value = value;
        }

        public override bool VisitElementHasValueExpr(ElementValueParser.ElementHasValueExprContext context)
        {
            string parsedField = context.field().GetText();
            string parsedValue = context.value().GetText().Trim('"');
            return parsedField.Equals(_field) && parsedValue.Equals(_value);
        }
    }
}
