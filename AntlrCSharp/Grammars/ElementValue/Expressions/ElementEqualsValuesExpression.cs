﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntlrCSharp.Grammars.ElementValue.Expressions
{
    public class ElementEqualsValuesExpression
    {
        public ElementEqualsValuesExpression(bool equals, string field, ValuesExpression valuesExpression) 
        {
            Equals = equals;
            Field = field;
            ValuesExpression = valuesExpression;
        }

        public bool Equals { get; }
        public string Field { get; }
        public ValuesExpression ValuesExpression { get; }
    }
}
