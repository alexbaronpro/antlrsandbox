﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntlrCSharp.Grammars.ElementValue.Expressions
{
    public class ValuesExpression
    {
        public ValuesExpression(List<string> values) 
        {
            Values = values;
        }

        public List<string> Values { get; }
    }
}
