﻿namespace AntlrCSharp.Grammars.ElementValue.Expressions
{
    public class ElementHasValueExpression
    {
        public ElementHasValueExpression(string element, string value) 
        {
            Element = element;
            Value = value;
        }

        public string Element { get; }
        public string Value { get; }

    }
}
