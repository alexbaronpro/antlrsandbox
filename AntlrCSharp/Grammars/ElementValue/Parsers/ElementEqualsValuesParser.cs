﻿using Antlr4.Runtime;
using AntlrCSharp.Grammars.ElementValue.Expressions;
using AntlrCSharp.Grammars.ElementValue.Visitors;

namespace AntlrCSharp.Grammars.ElementValue.Parsers
{
    public class ElementEqualsValuesParser
    {
        private readonly ElementEqualsValuesVisitor _visitor = new();

        public ElementEqualsValuesExpression Parse(string expression)
        {
            var charStream = new AntlrInputStream(expression);
            var lexer = new ElementValueLexer(charStream);
            var tokenStream = new CommonTokenStream(lexer);
            var parser = new ElementValueParser(tokenStream);
            var tree = parser.elementEqualsValuesExpr();
            var expr = tree.Accept(_visitor);

            return expr;
        }
    }
}
