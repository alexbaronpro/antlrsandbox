//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     ANTLR Version: 4.13.1
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generated from c:/Users/alexa/source/repos/antlrsandbox/AntlrCSharp/Grammars/ElementValue/ElementValueParser.g4 by ANTLR 4.13.1

// Unreachable code detected
#pragma warning disable 0162
// The variable '...' is assigned but its value is never used
#pragma warning disable 0219
// Missing XML comment for publicly visible type or member '...'
#pragma warning disable 1591
// Ambiguous reference in cref attribute
#pragma warning disable 419

using System;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using Antlr4.Runtime;
using Antlr4.Runtime.Atn;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
using DFA = Antlr4.Runtime.Dfa.DFA;

[System.CodeDom.Compiler.GeneratedCode("ANTLR", "4.13.1")]
[System.CLSCompliant(false)]
public partial class ElementValueParser : Parser {
	protected static DFA[] decisionToDFA;
	protected static PredictionContextCache sharedContextCache = new PredictionContextCache();
	public const int
		EQUALS=1, NOT_EQUALS=2, DOT=3, AND=4, OR=5, ELEMENT=6, HAS_VALUE=7, IDENTIFIER=8, 
		VALUE=9, WS=10;
	public const int
		RULE_expr = 0, RULE_elementHasValueExpr = 1, RULE_elementEqualsValuesExpr = 2, 
		RULE_field = 3, RULE_value = 4, RULE_values = 5;
	public static readonly string[] ruleNames = {
		"expr", "elementHasValueExpr", "elementEqualsValuesExpr", "field", "value", 
		"values"
	};

	private static readonly string[] _LiteralNames = {
		null, "'='", "'!='", "'.'", null, null, "'The Element'", "'has the value'"
	};
	private static readonly string[] _SymbolicNames = {
		null, "EQUALS", "NOT_EQUALS", "DOT", "AND", "OR", "ELEMENT", "HAS_VALUE", 
		"IDENTIFIER", "VALUE", "WS"
	};
	public static readonly IVocabulary DefaultVocabulary = new Vocabulary(_LiteralNames, _SymbolicNames);

	[NotNull]
	public override IVocabulary Vocabulary
	{
		get
		{
			return DefaultVocabulary;
		}
	}

	public override string GrammarFileName { get { return "ElementValueParser.g4"; } }

	public override string[] RuleNames { get { return ruleNames; } }

	public override int[] SerializedAtn { get { return _serializedATN; } }

	static ElementValueParser() {
		decisionToDFA = new DFA[_ATN.NumberOfDecisions];
		for (int i = 0; i < _ATN.NumberOfDecisions; i++) {
			decisionToDFA[i] = new DFA(_ATN.GetDecisionState(i), i);
		}
	}

		public ElementValueParser(ITokenStream input) : this(input, Console.Out, Console.Error) { }

		public ElementValueParser(ITokenStream input, TextWriter output, TextWriter errorOutput)
		: base(input, output, errorOutput)
	{
		Interpreter = new ParserATNSimulator(this, _ATN, decisionToDFA, sharedContextCache);
	}

	public partial class ExprContext : ParserRuleContext {
		[System.Diagnostics.DebuggerNonUserCode] public ElementHasValueExprContext elementHasValueExpr() {
			return GetRuleContext<ElementHasValueExprContext>(0);
		}
		[System.Diagnostics.DebuggerNonUserCode] public ElementEqualsValuesExprContext elementEqualsValuesExpr() {
			return GetRuleContext<ElementEqualsValuesExprContext>(0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState)
			: base(parent, invokingState)
		{
		}
		public override int RuleIndex { get { return RULE_expr; } }
		[System.Diagnostics.DebuggerNonUserCode]
		public override TResult Accept<TResult>(IParseTreeVisitor<TResult> visitor) {
			IElementValueParserVisitor<TResult> typedVisitor = visitor as IElementValueParserVisitor<TResult>;
			if (typedVisitor != null) return typedVisitor.VisitExpr(this);
			else return visitor.VisitChildren(this);
		}
	}

	[RuleVersion(0)]
	public ExprContext expr() {
		ExprContext _localctx = new ExprContext(Context, State);
		EnterRule(_localctx, 0, RULE_expr);
		try {
			State = 14;
			ErrorHandler.Sync(this);
			switch ( Interpreter.AdaptivePredict(TokenStream,0,Context) ) {
			case 1:
				EnterOuterAlt(_localctx, 1);
				{
				State = 12;
				elementHasValueExpr();
				}
				break;
			case 2:
				EnterOuterAlt(_localctx, 2);
				{
				State = 13;
				elementEqualsValuesExpr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			ErrorHandler.ReportError(this, re);
			ErrorHandler.Recover(this, re);
		}
		finally {
			ExitRule();
		}
		return _localctx;
	}

	public partial class ElementHasValueExprContext : ParserRuleContext {
		[System.Diagnostics.DebuggerNonUserCode] public ITerminalNode ELEMENT() { return GetToken(ElementValueParser.ELEMENT, 0); }
		[System.Diagnostics.DebuggerNonUserCode] public FieldContext field() {
			return GetRuleContext<FieldContext>(0);
		}
		[System.Diagnostics.DebuggerNonUserCode] public ITerminalNode HAS_VALUE() { return GetToken(ElementValueParser.HAS_VALUE, 0); }
		[System.Diagnostics.DebuggerNonUserCode] public ValueContext value() {
			return GetRuleContext<ValueContext>(0);
		}
		public ElementHasValueExprContext(ParserRuleContext parent, int invokingState)
			: base(parent, invokingState)
		{
		}
		public override int RuleIndex { get { return RULE_elementHasValueExpr; } }
		[System.Diagnostics.DebuggerNonUserCode]
		public override TResult Accept<TResult>(IParseTreeVisitor<TResult> visitor) {
			IElementValueParserVisitor<TResult> typedVisitor = visitor as IElementValueParserVisitor<TResult>;
			if (typedVisitor != null) return typedVisitor.VisitElementHasValueExpr(this);
			else return visitor.VisitChildren(this);
		}
	}

	[RuleVersion(0)]
	public ElementHasValueExprContext elementHasValueExpr() {
		ElementHasValueExprContext _localctx = new ElementHasValueExprContext(Context, State);
		EnterRule(_localctx, 2, RULE_elementHasValueExpr);
		try {
			EnterOuterAlt(_localctx, 1);
			{
			State = 16;
			Match(ELEMENT);
			State = 17;
			field();
			State = 18;
			Match(HAS_VALUE);
			State = 19;
			value();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			ErrorHandler.ReportError(this, re);
			ErrorHandler.Recover(this, re);
		}
		finally {
			ExitRule();
		}
		return _localctx;
	}

	public partial class ElementEqualsValuesExprContext : ParserRuleContext {
		[System.Diagnostics.DebuggerNonUserCode] public ITerminalNode ELEMENT() { return GetToken(ElementValueParser.ELEMENT, 0); }
		[System.Diagnostics.DebuggerNonUserCode] public FieldContext field() {
			return GetRuleContext<FieldContext>(0);
		}
		[System.Diagnostics.DebuggerNonUserCode] public ValuesContext @values() {
			return GetRuleContext<ValuesContext>(0);
		}
		[System.Diagnostics.DebuggerNonUserCode] public ITerminalNode EQUALS() { return GetToken(ElementValueParser.EQUALS, 0); }
		[System.Diagnostics.DebuggerNonUserCode] public ITerminalNode NOT_EQUALS() { return GetToken(ElementValueParser.NOT_EQUALS, 0); }
		public ElementEqualsValuesExprContext(ParserRuleContext parent, int invokingState)
			: base(parent, invokingState)
		{
		}
		public override int RuleIndex { get { return RULE_elementEqualsValuesExpr; } }
		[System.Diagnostics.DebuggerNonUserCode]
		public override TResult Accept<TResult>(IParseTreeVisitor<TResult> visitor) {
			IElementValueParserVisitor<TResult> typedVisitor = visitor as IElementValueParserVisitor<TResult>;
			if (typedVisitor != null) return typedVisitor.VisitElementEqualsValuesExpr(this);
			else return visitor.VisitChildren(this);
		}
	}

	[RuleVersion(0)]
	public ElementEqualsValuesExprContext elementEqualsValuesExpr() {
		ElementEqualsValuesExprContext _localctx = new ElementEqualsValuesExprContext(Context, State);
		EnterRule(_localctx, 4, RULE_elementEqualsValuesExpr);
		int _la;
		try {
			EnterOuterAlt(_localctx, 1);
			{
			State = 21;
			Match(ELEMENT);
			State = 22;
			field();
			State = 23;
			_la = TokenStream.LA(1);
			if ( !(_la==EQUALS || _la==NOT_EQUALS) ) {
			ErrorHandler.RecoverInline(this);
			}
			else {
				ErrorHandler.ReportMatch(this);
			    Consume();
			}
			State = 24;
			@values();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			ErrorHandler.ReportError(this, re);
			ErrorHandler.Recover(this, re);
		}
		finally {
			ExitRule();
		}
		return _localctx;
	}

	public partial class FieldContext : ParserRuleContext {
		[System.Diagnostics.DebuggerNonUserCode] public ITerminalNode[] IDENTIFIER() { return GetTokens(ElementValueParser.IDENTIFIER); }
		[System.Diagnostics.DebuggerNonUserCode] public ITerminalNode IDENTIFIER(int i) {
			return GetToken(ElementValueParser.IDENTIFIER, i);
		}
		[System.Diagnostics.DebuggerNonUserCode] public ITerminalNode[] DOT() { return GetTokens(ElementValueParser.DOT); }
		[System.Diagnostics.DebuggerNonUserCode] public ITerminalNode DOT(int i) {
			return GetToken(ElementValueParser.DOT, i);
		}
		public FieldContext(ParserRuleContext parent, int invokingState)
			: base(parent, invokingState)
		{
		}
		public override int RuleIndex { get { return RULE_field; } }
		[System.Diagnostics.DebuggerNonUserCode]
		public override TResult Accept<TResult>(IParseTreeVisitor<TResult> visitor) {
			IElementValueParserVisitor<TResult> typedVisitor = visitor as IElementValueParserVisitor<TResult>;
			if (typedVisitor != null) return typedVisitor.VisitField(this);
			else return visitor.VisitChildren(this);
		}
	}

	[RuleVersion(0)]
	public FieldContext field() {
		FieldContext _localctx = new FieldContext(Context, State);
		EnterRule(_localctx, 6, RULE_field);
		int _la;
		try {
			EnterOuterAlt(_localctx, 1);
			{
			State = 26;
			Match(IDENTIFIER);
			State = 31;
			ErrorHandler.Sync(this);
			_la = TokenStream.LA(1);
			while (_la==DOT) {
				{
				{
				State = 27;
				Match(DOT);
				State = 28;
				Match(IDENTIFIER);
				}
				}
				State = 33;
				ErrorHandler.Sync(this);
				_la = TokenStream.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			ErrorHandler.ReportError(this, re);
			ErrorHandler.Recover(this, re);
		}
		finally {
			ExitRule();
		}
		return _localctx;
	}

	public partial class ValueContext : ParserRuleContext {
		[System.Diagnostics.DebuggerNonUserCode] public ITerminalNode VALUE() { return GetToken(ElementValueParser.VALUE, 0); }
		public ValueContext(ParserRuleContext parent, int invokingState)
			: base(parent, invokingState)
		{
		}
		public override int RuleIndex { get { return RULE_value; } }
		[System.Diagnostics.DebuggerNonUserCode]
		public override TResult Accept<TResult>(IParseTreeVisitor<TResult> visitor) {
			IElementValueParserVisitor<TResult> typedVisitor = visitor as IElementValueParserVisitor<TResult>;
			if (typedVisitor != null) return typedVisitor.VisitValue(this);
			else return visitor.VisitChildren(this);
		}
	}

	[RuleVersion(0)]
	public ValueContext value() {
		ValueContext _localctx = new ValueContext(Context, State);
		EnterRule(_localctx, 8, RULE_value);
		try {
			EnterOuterAlt(_localctx, 1);
			{
			State = 34;
			Match(VALUE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			ErrorHandler.ReportError(this, re);
			ErrorHandler.Recover(this, re);
		}
		finally {
			ExitRule();
		}
		return _localctx;
	}

	public partial class ValuesContext : ParserRuleContext {
		[System.Diagnostics.DebuggerNonUserCode] public ValueContext[] value() {
			return GetRuleContexts<ValueContext>();
		}
		[System.Diagnostics.DebuggerNonUserCode] public ValueContext value(int i) {
			return GetRuleContext<ValueContext>(i);
		}
		[System.Diagnostics.DebuggerNonUserCode] public ITerminalNode[] AND() { return GetTokens(ElementValueParser.AND); }
		[System.Diagnostics.DebuggerNonUserCode] public ITerminalNode AND(int i) {
			return GetToken(ElementValueParser.AND, i);
		}
		[System.Diagnostics.DebuggerNonUserCode] public ITerminalNode[] OR() { return GetTokens(ElementValueParser.OR); }
		[System.Diagnostics.DebuggerNonUserCode] public ITerminalNode OR(int i) {
			return GetToken(ElementValueParser.OR, i);
		}
		public ValuesContext(ParserRuleContext parent, int invokingState)
			: base(parent, invokingState)
		{
		}
		public override int RuleIndex { get { return RULE_values; } }
		[System.Diagnostics.DebuggerNonUserCode]
		public override TResult Accept<TResult>(IParseTreeVisitor<TResult> visitor) {
			IElementValueParserVisitor<TResult> typedVisitor = visitor as IElementValueParserVisitor<TResult>;
			if (typedVisitor != null) return typedVisitor.VisitValues(this);
			else return visitor.VisitChildren(this);
		}
	}

	[RuleVersion(0)]
	public ValuesContext @values() {
		ValuesContext _localctx = new ValuesContext(Context, State);
		EnterRule(_localctx, 10, RULE_values);
		int _la;
		try {
			EnterOuterAlt(_localctx, 1);
			{
			State = 36;
			value();
			State = 42;
			ErrorHandler.Sync(this);
			_la = TokenStream.LA(1);
			while (_la==AND || _la==OR) {
				{
				State = 40;
				ErrorHandler.Sync(this);
				switch (TokenStream.LA(1)) {
				case AND:
					{
					State = 37;
					Match(AND);
					}
					break;
				case OR:
					{
					State = 38;
					Match(OR);
					State = 39;
					value();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				State = 44;
				ErrorHandler.Sync(this);
				_la = TokenStream.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			ErrorHandler.ReportError(this, re);
			ErrorHandler.Recover(this, re);
		}
		finally {
			ExitRule();
		}
		return _localctx;
	}

	private static int[] _serializedATN = {
		4,1,10,46,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,1,0,1,0,3,0,
		15,8,0,1,1,1,1,1,1,1,1,1,1,1,2,1,2,1,2,1,2,1,2,1,3,1,3,1,3,5,3,30,8,3,
		10,3,12,3,33,9,3,1,4,1,4,1,5,1,5,1,5,1,5,5,5,41,8,5,10,5,12,5,44,9,5,1,
		5,0,0,6,0,2,4,6,8,10,0,1,1,0,1,2,43,0,14,1,0,0,0,2,16,1,0,0,0,4,21,1,0,
		0,0,6,26,1,0,0,0,8,34,1,0,0,0,10,36,1,0,0,0,12,15,3,2,1,0,13,15,3,4,2,
		0,14,12,1,0,0,0,14,13,1,0,0,0,15,1,1,0,0,0,16,17,5,6,0,0,17,18,3,6,3,0,
		18,19,5,7,0,0,19,20,3,8,4,0,20,3,1,0,0,0,21,22,5,6,0,0,22,23,3,6,3,0,23,
		24,7,0,0,0,24,25,3,10,5,0,25,5,1,0,0,0,26,31,5,8,0,0,27,28,5,3,0,0,28,
		30,5,8,0,0,29,27,1,0,0,0,30,33,1,0,0,0,31,29,1,0,0,0,31,32,1,0,0,0,32,
		7,1,0,0,0,33,31,1,0,0,0,34,35,5,9,0,0,35,9,1,0,0,0,36,42,3,8,4,0,37,41,
		5,4,0,0,38,39,5,5,0,0,39,41,3,8,4,0,40,37,1,0,0,0,40,38,1,0,0,0,41,44,
		1,0,0,0,42,40,1,0,0,0,42,43,1,0,0,0,43,11,1,0,0,0,44,42,1,0,0,0,4,14,31,
		40,42
	};

	public static readonly ATN _ATN =
		new ATNDeserializer().Deserialize(_serializedATN);


}
