// Generated from c:/Users/ylatif/source/repos/antlrsandbox/AntlrCSharp/Grammars/ElementValue/ElementValue.g4 by ANTLR 4.13.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ElementValueParser}.
 */
public interface ElementValueListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ElementValueParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(ElementValueParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link ElementValueParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(ElementValueParser.ExprContext ctx);
}