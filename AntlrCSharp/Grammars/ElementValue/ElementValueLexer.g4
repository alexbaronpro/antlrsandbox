lexer grammar ElementValueLexer;



EQUALS: '=';
NOT_EQUALS: '!=';
DOT: '.';

AND: A N D;
OR: O R;
ELEMENT: 'The Element';
HAS_VALUE: 'has the value';

IDENTIFIER: [a-zA-Z_][a-zA-Z0-9_]*;
VALUE: '"' (~["])* '"';
WS: [ \t\r\n]+ -> skip;

fragment A: [Aa];
fragment B: [Bb];
fragment C: [Cc];
fragment D: [Dd];
fragment E: [Ee];
fragment F: [Ff];
fragment G: [Gg];
fragment H: [Hh];
fragment I: [Ii];
fragment J: [Jj];
fragment K: [Kk];
fragment L: [Ll];
fragment M: [Mm];
fragment N: [Nn];
fragment O: [Oo];
fragment P: [Pp];
fragment Q: [Qq];
fragment R: [Rr];
fragment S: [Ss];
fragment T: [Tt];
fragment U: [Uu];
fragment V: [Vv];
fragment W: [Ww];
fragment X: [Xx];
fragment Y: [Yy];
fragment Z: [Zz];