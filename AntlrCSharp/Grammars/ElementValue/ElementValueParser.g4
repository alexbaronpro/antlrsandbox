parser grammar ElementValueParser;

options { tokenVocab=ElementValueLexer; }

expr: elementHasValueExpr | elementEqualsValuesExpr;

elementHasValueExpr: ELEMENT field HAS_VALUE value;
elementEqualsValuesExpr: ELEMENT field (EQUALS | NOT_EQUALS) values;

field: IDENTIFIER (DOT IDENTIFIER)*;
value: VALUE;
values: value (AND | OR value)*;