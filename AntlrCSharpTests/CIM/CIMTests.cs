﻿using Antlr4.Runtime;
using AntlrCSharp.Grammars.CIM;
using AntlrCSharp.Grammars.ElementValue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntlrCSharpTests.CIM
{
    [TestClass]
    public class CIMTests
    {
        [TestMethod]
        public void ApplyDateTimeEqualsOneDay()
        {
            var condition = "period.timeInterval.end.Day - period.timeInterval.start.Day == 1";

            DateTime dateTimeStart = new(2024, 1, 1);
            DateTime dateTimeEnd = new(2024, 1, 2);

            var inputStream = new AntlrInputStream(condition);
            var lexer = new ExpressionLexer(inputStream);
            var tokenStream = new CommonTokenStream(lexer);
            var parser = new CIMParser(tokenStream);
            var context = parser.expr();

            var evaluator = new CIMEvaluator(dateTimeStart, dateTimeEnd);
            var result = evaluator.Visit(context) == 1;

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ElementHasValueTest()
        {
            var rule = "The Element receiver_MarketParticipant.mRID has the value \"10X1001A1001A094\"";

            var field = "receiver_MarketParticipant.mRID";
            var value = "10X1001A1001A094";

            var inputStream = new AntlrInputStream(rule);
            var lexer = new ElementValueLexer(inputStream);
            var tokenStream = new CommonTokenStream(lexer);
            var parser = new ElementValueParser(tokenStream);
            var context = parser.expr();

            var evaluator = new ElementValueEvaluator(field, value);
            var result = evaluator.Visit(context);

            Assert.IsTrue(result);
        }
    }
}
