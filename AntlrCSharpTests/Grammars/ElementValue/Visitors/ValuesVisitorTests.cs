﻿using Antlr4.Runtime;
using FluentAssertions;

namespace AntlrCSharp.Grammars.ElementValue.Visitors.Tests
{
    [TestClass()]
    public class ValuesVisitorTests
    {
        [TestMethod()]
        public void VisitValuesWithoutORTest()
        {
            var rule = "\"10X1001A1001A094\"";

            var inputStream = new AntlrInputStream(rule);
            var lexer = new ElementValueLexer(inputStream);
            var tokenStream = new CommonTokenStream(lexer);
            var parser = new ElementValueParser(tokenStream);
            var context = parser.values();

            var evaluator = new ValuesVisitor();
            var result = evaluator.Visit(context);

            result.Values.Should().HaveCount(1);
            result.Values.Contains("10X1001A1001A094");
        }

        [TestMethod()]
        public void VisitValuesWithManyORTest()
        {
            var rule = "\"10X1001A1001A094\" OR \"10X1001A1002A094\"";

            var inputStream = new AntlrInputStream(rule);
            var lexer = new ElementValueLexer(inputStream);
            var tokenStream = new CommonTokenStream(lexer);
            var parser = new ElementValueParser(tokenStream);
            var context = parser.values();

            var evaluator = new ValuesVisitor();
            var result = evaluator.Visit(context);

            result.Values.Should().HaveCount(2);
            result.Values.Contains("10X1001A1001A094");
            result.Values.Contains("10X1001A1002A094");
        }
    }
}