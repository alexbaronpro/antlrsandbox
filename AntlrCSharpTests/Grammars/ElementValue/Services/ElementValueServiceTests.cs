﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AntlrCSharp.Grammars.ElementValue.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr4.Runtime;

namespace AntlrCSharp.Grammars.ElementValue.Services.Tests
{
    [TestClass()]
    public class ElementValueServiceTests
    {
        [DataTestMethod()]
        [DataRow("The Element receiver_MarketParticipant.mRID has the value \"10X1001A1001A094\"", "receiver_MarketParticipant.mRID", "10X1001A1001A094")]
        [DataRow("The Element domain.mRID has the value \"10YBE----------2\"", "domain.mRID", "10YBE----------2")]
        [DataRow("The Element type has the value \"A01\"", "type", "A01")]
        [DataRow("The Element process.processType has the value \"A02\"", "process.processType", "A02")]
        [DataRow("The Element sender_MarketParticipant.marketRole.type has the value \"A08\"", "sender_MarketParticipant.marketRole.type", "A08")]
        [DataRow("The Element receiver_MarketParticipant.marketRole.type has the value \"A04\"", "receiver_MarketParticipant.marketRole.type", "A04")]
        public void ExecuteHasValueTest(string expression, string field, string value)
        {
            ElementValueService service = new();

            var result = service.ExecuteHasValue(field, value, expression);

            Assert.IsTrue(result);
        }

        [DataTestMethod()]
        [DataRow("The Element processtype =\"A17\"", "A17")]
        [DataRow("The Element processtype !=\"A17\"", "A18")]
        [DataRow("The Element processtype =\"A17\" or \"A18\"", "A17")]
        [DataRow("The Element processtype =\"A17\" OR \"A18\"", "A18")]
        [DataRow("The Element processtype !=\"A17\" or \"A18\"", "A19")]
        public void ExecuteEqualsValuesTest(string expression, string value)
        {
            ElementValueService service = new();

            var result = service.ExecuteEqualsValues(value, expression);

            Assert.IsTrue(result);
        }
    }
}