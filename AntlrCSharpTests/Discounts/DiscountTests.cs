﻿using AntlrCSharp.Grammars.Discounts;
using AntlrCSharp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntlrCSharpTests.Discounts
{
    [TestClass]
    public class DiscountTests
    {
        [TestMethod]
        public void ApplyDiscountToAnyBasket()
        {
            var discount = "APPLY 10% DISCOUNT";
            var basket = new Basket(new List<Product>
            {
                new("tshirt", 50),
                new("jeans", 100),
                new("jacket", 200)
            });

            var calculator = new DiscountCalculator();
            calculator.ApplyDiscount(basket, discount);

            Assert.AreEqual(350, basket.OriginalPrice);
            Assert.AreEqual(35, basket.Discount);
            Assert.AreEqual(315, basket.TotalPrice);
        }

        [TestMethod]
        public void ApplyDiscountToJeansOnly()
        {
            var discount = "APPLY 10% DISCOUNT TO jeans";
            var basket = new Basket(new List<Product>
            {
                new Product("tshirt", 50),
                new Product("jeans", 100),
                new Product("jacket", 200)
            });

            var calculator = new DiscountCalculator();
            calculator.ApplyDiscount(basket, discount);

            Assert.AreEqual(350, basket.OriginalPrice);
            Assert.AreEqual(10, basket.Discount);
            Assert.AreEqual(340, basket.TotalPrice);
        }

        [TestMethod]
        public void ApplyComplexDiscount()
        {
            var discount = @"APPLY 10% DISCOUNT TO jeans
                             APPLY 50% DISCOUNT TO jacket";
            var basket = new Basket(new List<Product>
            {
                new Product("tshirt", 50),
                new Product("jeans", 100),
                new Product("jacket", 200)
            });

            var calculator = new DiscountCalculator();
            calculator.ApplyDiscount(basket, discount);

            Assert.AreEqual(350, basket.OriginalPrice);
            Assert.AreEqual(10 + 100, basket.Discount);
            Assert.AreEqual(240, basket.TotalPrice);
        }

        [TestMethod]
        public void ApplyComplexDiscountSuccessfullyWithPrecondition()
        {
            var discount = @"FOR BASKET WITH tshirt
                             APPLY 10% DISCOUNT TO jeans
                             APPLY 50% DISCOUNT TO jacket";
            var basket = new Basket(new List<Product>
            {
                new Product("tshirt", 50),
                new Product("jeans", 100),
                new Product("jacket", 200)
            });

            var calculator = new DiscountCalculator();
            calculator.ApplyDiscount(basket, discount);

            Assert.AreEqual(350, basket.OriginalPrice);
            Assert.AreEqual(10 + 100, basket.Discount);
            Assert.AreEqual(240, basket.TotalPrice);
        }

        [TestMethod]
        public void FailOnPrecondition()
        {
            var discount = @"FOR BASKET WITH nonExistingProduct
                             APPLY 10% DISCOUNT TO jeans
                             APPLY 50% DISCOUNT TO jacket";

            var basket = new Basket(new List<Product>
            {
                new Product("tshirt", 50),
                new Product("jeans", 100),
                new Product("jacket", 200)
            });

            var calculator = new DiscountCalculator();
            Assert.ThrowsException<InvalidOperationException>(() => calculator.ApplyDiscount(basket, discount));
        }
    }
}
